package com.ashwilliams87.first.interfaces;

import com.ashwilliams87.first.answers.StatExerciseAnswer;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface Api {

    @POST("/stat")
    @FormUrlEncoded
    Call<StatExerciseAnswer> sendStatPOST(@Field("exercise_id") long exercise_id,
                                          @Field("date") String date);

    @GET("/exercises")
    @FormUrlEncoded
    Call<StatExerciseAnswer> getExercisesGET(@Field("exercise_id") long exercise_id,
                                             @Field("date") String date);
}