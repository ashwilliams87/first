package com.ashwilliams87.first.interfaces;

public interface Observer<T> {
    public void update(T value);
}
