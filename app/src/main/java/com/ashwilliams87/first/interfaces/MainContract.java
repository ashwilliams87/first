package com.ashwilliams87.first.interfaces;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;

public interface MainContract {

    interface View {
        void showText(String message);

        void showTime(String time);

        void setExerciseImageView(Bitmap image);

        int getExerciseImageViewId(int imageNumber);

        String getTimerValue();
    }

    interface Presenter {
        void onClickStartButton();

        void onClickResetButton();

        void onClickTimerUpButton();

        void onClickTimeDownButton();

        void onClickNextExerciseButton();

        void onClickPrevExerciseButton();

        void onClickTest1Button();

        void onClickTest2Button();

        void onDestroy();

        void initDatabase();
    }

    interface Repository {
        public String loadMessage();

        public Bitmap getExerciseBitmap();

        public void initExerciseDatabase();

    }
}
