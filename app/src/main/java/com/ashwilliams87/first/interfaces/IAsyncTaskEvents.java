package com.ashwilliams87.first.interfaces;

public interface IAsyncTaskEvents {
    void onPreExecute();

    void onPostExecute();

    void onProgressUpdate(Integer integer);

    void onCancel();

    void cancel();
}