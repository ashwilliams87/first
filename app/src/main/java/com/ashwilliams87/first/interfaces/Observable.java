package com.ashwilliams87.first.interfaces;

public interface Observable<T> {
    public void registerObserver(Observer observer);

    public void removeObserver();

    public void notifyObserver(T value);
}
