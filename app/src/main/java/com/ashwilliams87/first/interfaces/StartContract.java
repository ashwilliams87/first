package com.ashwilliams87.first.interfaces;

public interface StartContract {
    
    interface Presenter {

        void initDatabase();

        void onDestroy();
    }
}
