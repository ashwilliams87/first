package com.ashwilliams87.first.handlers;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.widget.ImageView;

public class ExerciseImageHandler extends Handler {
    private ImageView exerciseImageView;

    public ExerciseImageHandler(ImageView exerciseImageView) {
        this.exerciseImageView = exerciseImageView;
    }

    public void handleMessage(Message msg) {
        Bitmap photoTask = (Bitmap) msg.obj;
        // обновляем TextView
        exerciseImageView.setImageBitmap(photoTask);
    }
}
