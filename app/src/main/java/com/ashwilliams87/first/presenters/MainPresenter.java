package com.ashwilliams87.first.presenters;

import android.content.Context;
import android.graphics.Bitmap;
import android.location.LocationManager;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.ashwilliams87.first.entities.UserEntity;
import com.ashwilliams87.first.handlers.ExerciseImageHandler;
import com.ashwilliams87.first.interactors.ExerciseImage;
import com.ashwilliams87.first.interactors.ExerciseTimer;
import com.ashwilliams87.first.interfaces.MainContract;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Observable;
import java.util.Observer;

//import MainRepository;
//import CounterAsyncTask;
//import IAsyncTaskEvents;

public class MainPresenter implements MainContract.Presenter, Observer {

    private static final String TAG = "MainPresenter";
    private static Integer defaultTime;

    //Компоненты MVP приложения
    private MainContract.View mView;

    private ExerciseTimer exerciseTimerInteractor;
    private ExerciseImage exerciseInteractor;
    private ExerciseImageHandler exerciseImageHandler;

    public MainPresenter(MainContract.View mView, ExerciseTimer exerciseTimerInteractor, ExerciseImage exerciseInteractor, ExerciseImageHandler exerciseImageHandler) {
        this.mView = mView;
        this.exerciseTimerInteractor = exerciseTimerInteractor;
        this.exerciseInteractor = exerciseInteractor;
        this.exerciseInteractor.addObserver(this);
        this.exerciseImageHandler = exerciseImageHandler;

        //MainPresenter.defaultTime = exerciseTimerInteractor.getTime();
        MainPresenter.defaultTime = 65;
        //this.mainRepository = new MainRepository();

        Log.d(TAG, "Constructor");
    }

    @Override
    public void onClickStartButton() {
        //Toast.makeText(this, "startAsync", Toast.LENGTH_SHORT).show();
        exerciseTimerInteractor = new ExerciseTimer(defaultTime);
        exerciseTimerInteractor.addObserver(this);
        exerciseTimerInteractor.execute();
    }

    @Override
    public void onClickResetButton() {
        if (exerciseTimerInteractor != null) {
            exerciseTimerInteractor.cancel();
        }
        int id = 0;
        mView.showTime(defaultTime.toString());
        exerciseInteractor.initExerciseDbQuery(1);
        //mView.setExerciseImageView(1);
        // Toast.makeText((Context) mView, "Время установлено", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClickTimerUpButton() {
        if (exerciseTimerInteractor != null) {
            exerciseTimerInteractor.addTime(5);
            mView.showTime(exerciseTimerInteractor.getTime().toString());
        } else {
            defaultTime += 5;
            String t = defaultTime.toString();
            Log.d(TAG, "onClickTimeUp()");
            Log.d(TAG, t);
            mView.showTime(defaultTime.toString());
        }
        Toast.makeText((Context) mView, "Добавлено 5 секунд", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onClickTimeDownButton() {
        if (exerciseTimerInteractor != null) {
            exerciseTimerInteractor.removeTime(5);
            mView.showTime(exerciseTimerInteractor.getTime().toString());
        } else {
            defaultTime = (defaultTime - 5) > 0 ? defaultTime - 5 : 0;
            mView.showTime(defaultTime.toString());
        }
        Toast.makeText((Context) mView, "Уменьшено на 5 секунд", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClickNextExerciseButton() {

        if (exerciseTimerInteractor != null) {
            exerciseTimerInteractor.cancel();
            //mView.showTime(defaultTime.toString());
        }
        //Log.d(TAG, String.valueOf(exerciseInteractor.getCurrentExerciseId()));
        // Log.d(TAG, String.valueOf(exerciseInteractor.getCountImgMax()));
        if (exerciseInteractor.getCurrentExerciseId() < exerciseInteractor.getCountImgMax()) {
            Integer i = exerciseInteractor.getCurrentExerciseId();
            exerciseInteractor.setCurrentExerciseId(++i);
            exerciseInteractor.initExerciseDbQuery(exerciseInteractor.getCurrentExerciseId());
        } else {
            Toast.makeText((Context) mView, "Больше нет упражнений!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClickPrevExerciseButton() {

        if (exerciseTimerInteractor != null) {
            exerciseTimerInteractor.cancel();
            mView.showTime(defaultTime.toString());
        }

        if (exerciseInteractor.getCurrentExerciseId() > exerciseInteractor.getCountImg()) {
            Integer i = exerciseInteractor.getCurrentExerciseId();
            exerciseInteractor.setCurrentExerciseId(--i);
            exerciseInteractor.initExerciseDbQuery(exerciseInteractor.getCurrentExerciseId());
        } else {
            Toast.makeText((Context) mView, "Больше нет упражнений!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClickTest1Button() {
        //  new Firebase("https://<your-firebase>/currentUsers");
        FirebaseDatabase mFirebaseDb = FirebaseDatabase.getInstance();
        DatabaseReference myRef = mFirebaseDb.getReference();
        UserEntity user = new UserEntity("emil", "ash_williams@mail.ru");
        myRef.child("users").child("abc").setValue(user);
        myRef.child("users").child("abd").setValue(user);
        myRef.child("users").child("abf").setValue(user);
        myRef.child("users").child("abg").setValue(user);
        Log.d(TAG, "пришел");
    }

    @Override
    public void onClickTest2Button() {

//        FirebaseDatabase mFirebaseDb = FirebaseDatabase.getInstance();
//        DatabaseReference myRef = mFirebaseDb.getReference();
//        UserEntity user = new UserEntity("emil", "ash_williams@mail.ru");
//
//        DatabaseReference myRef.child("users").child("abc");
//        Log.d(TAG, key);
//        key = myRef.child("users").child("1").getKey();
//        Log.d(TAG, key);
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy()");
        if (exerciseTimerInteractor != null) {
            exerciseTimerInteractor.cancel();
            exerciseTimerInteractor.deleteObserver(this);
            exerciseTimerInteractor = null;
        }
    }

    @Override
    public void initDatabase() {
        Log.d(TAG, "InitDb Presenter");
        exerciseInteractor.addObserver(this);
        exerciseInteractor.init();
    }

    @Override
    public void update(Observable subject, Object arg) {
        Log.d(TAG, "update");

        if (subject instanceof ExerciseTimer) {
            ExerciseTimer exerciseTimer = (ExerciseTimer) subject;
            mView.showTime(exerciseTimer.getTime().toString());
            Log.d(TAG, exerciseTimer.getTime().toString());

            if ((Integer.toString(exerciseTimer.getTime())).equals("0")) {
                exerciseInteractor.initExerciseDbQuery(1);
                Toast.makeText((Context) mView, "Время закончилось!", Toast.LENGTH_SHORT).show();
            }
        }

        if (subject instanceof ExerciseImage) {
            ExerciseImage exercise = (ExerciseImage) subject;
            Bitmap bt = exercise.getExerciseBitmap();
            Message msg = new Message();
            msg.obj = bt;
            exerciseImageHandler.sendMessage(msg);
        }
    }

}
