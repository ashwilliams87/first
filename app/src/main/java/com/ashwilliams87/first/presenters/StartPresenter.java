package com.ashwilliams87.first.presenters;

import android.util.Log;

import com.ashwilliams87.first.interfaces.StartContract;

import java.util.Observable;
import java.util.Observer;

import static android.support.constraint.Constraints.TAG;

public class StartPresenter implements StartContract.Presenter, Observer {

    @Override
    public void initDatabase() {
        Log.d(TAG, "InitDb Presenter");
        //exerciseInteractor.addObserver(this);
        //exerciseInteractor.init();
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void update(Observable o, Object arg) {
        Log.d(TAG, "update");
    }
}
