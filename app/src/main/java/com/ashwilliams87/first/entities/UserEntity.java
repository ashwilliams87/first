package com.ashwilliams87.first.entities;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class UserEntity{

    public String username;
    public String email;

    public UserEntity() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public UserEntity(String username, String email) {
        this.username = username;
        this.email = email;
    }

}