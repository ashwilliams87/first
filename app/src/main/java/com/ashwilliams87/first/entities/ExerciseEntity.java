package com.ashwilliams87.first.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "exercise")
public class ExerciseEntity {

    @PrimaryKey(autoGenerate = true)
    private long id;

    @ColumnInfo(name = "exerciseName")
    private String exerciseName;

    @ColumnInfo(name = "exerciseImage", typeAffinity = ColumnInfo.BLOB)
    private byte[] exerciseImage;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getExerciseName() {
        return this.exerciseName;
    }

    public void setExerciseName(String name) {
        this.exerciseName = name;
    }

    public byte[] getExerciseImage() {
        return this.exerciseImage;
    }

    public void setExerciseImage(byte[] exerciseImage) {
        this.exerciseImage = exerciseImage;
    }
}