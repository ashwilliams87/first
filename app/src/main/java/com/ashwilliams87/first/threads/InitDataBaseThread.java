package com.ashwilliams87.first.threads;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import android.util.Log;

import com.ashwilliams87.first.R;
import com.ashwilliams87.first.entities.ExerciseEntity;
import com.ashwilliams87.first.interactors.ExerciseImage;

import java.io.ByteArrayOutputStream;

public class InitDataBaseThread extends Thread {

    private ExerciseImage intercator;
    private Context context;

    public InitDataBaseThread() {
        this.intercator = intercator;
        //сделать лист ресурсов в активити и передать сюда лист, избавится о контекста.
        this.context = intercator.getContext();
    }

    public void run() {
        Log.d("InitExerciseDatabase", "InitDB Thread");
        // ExerciseEntity exercise = rep.exerciseDao().findById(3);
        //Log.d(TAG, exercise.getExerciseName());
//        Blob b = exercise.getExerciseImage();
//        int blobLength = (int) b.length();
//        byte[] blobAsBytes = b.getBytes(1, blobLength);
        //Bitmap btm = BitmapFactory.decodeByteArray(blobAsBytes, 0, blobAsBytes.length);

        ExerciseEntity[] exercises = new ExerciseEntity[11];
        for (int i = 0; i < 11; i++) {
            ExerciseEntity exercise = new ExerciseEntity();
            exercise.setId(i + 1);
            exercise.setExerciseName(String.valueOf(i + 1));

            //получаем картинку из ресурса в блоб
            int imageId = context.getResources().getIdentifier(context.getResources().getString(R.string.default_exercise_path) + String.valueOf(i + 1), null, context.getPackageName());
            Bitmap yourBitmap = BitmapFactory.decodeResource(context.getResources(), imageId);
            ByteArrayOutputStream blobOutputStream = new ByteArrayOutputStream();
            yourBitmap.compress(Bitmap.CompressFormat.JPEG, 100, blobOutputStream);
            byte[] blobImage = blobOutputStream.toByteArray();
            exercise.setExerciseImage(blobImage);

            exercises[i] = exercise;
        }

        intercator.getExerciseRepository().exerciseDao().insertAll(exercises);
        intercator.initEnd();
    }
}
