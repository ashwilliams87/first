package com.ashwilliams87.first.threads;

import android.content.Context;
import android.graphics.BitmapFactory;

import android.util.Log;

import com.ashwilliams87.first.entities.ExerciseEntity;
import com.ashwilliams87.first.interactors.ExerciseImage;

public class ExerciseQueryThread extends Thread {

    private ExerciseImage intercator;
    private Context context;
    private Integer exerciseId;

    public ExerciseQueryThread(ExerciseImage intercator, Integer id) {
        this.intercator = intercator;
        this.context = intercator.getContext();
        this.exerciseId = id;
    }

    public void run() {
        Log.d("InitExerciseDatabase", "InitDB Thread");
        ExerciseEntity exercise = intercator.getExerciseRepository().exerciseDao().findById((long) exerciseId);
        byte[] byteArray = exercise.getExerciseImage();
        this.intercator.endInitExerciseDbQuery(BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length), (int) (long) exercise.getId());
    }
}
