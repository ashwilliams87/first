package com.ashwilliams87.first.repositories;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.ashwilliams87.first.daos.ExerciseDao;
import com.ashwilliams87.first.entities.ExerciseEntity;

@Database(entities = {ExerciseEntity.class}, version = 1)
public abstract class ExerciseRepository extends RoomDatabase {

    private static ExerciseRepository INSTANCE;

    public abstract ExerciseDao exerciseDao();

    public static ExerciseRepository getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), ExerciseRepository.class, "exercise-database")
                            // allow queries on the main thread.
                            // Don't do this on a real app! See PersistenceBasicSample for an example.
                            .allowMainThreadQueries()
                            .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }
}