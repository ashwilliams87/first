package com.ashwilliams87.first.interactors;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.ashwilliams87.first.threads.ExerciseQueryThread;
import com.ashwilliams87.first.threads.InitDataBaseThread;
import com.ashwilliams87.first.repositories.ExerciseRepository;

import java.util.Observable;

public class ExerciseImage extends Observable {

    private static final String TAG = "ExerciseImageInteractor";
    private Context context;

    private Integer currentExerciseId;
    private Bitmap currentExerciseImage;
    private static final int countImg = 1;
    private static final int countImgMax = 11;

    private ExerciseRepository exerciseRepository;

    public ExerciseImage(Context context, ExerciseRepository exerciseRepository) {
        this.context = context;
        this.exerciseRepository = exerciseRepository;
        this.currentExerciseId = 1;
    }

    public Context getContext() {
        return context;
    }


    public ExerciseRepository getExerciseRepository() {
        return exerciseRepository;
    }


    public void init() {

    }

    public void initEnd() {
        this.setChanged();
        notifyObservers();
    }

    public void initExerciseDbQuery(int id) {
        ExerciseQueryThread myThread = new ExerciseQueryThread(this, id);
        myThread.start();
    }

    public void endInitExerciseDbQuery(Bitmap resultImage, Integer id) {
        currentExerciseImage = resultImage;
        currentExerciseId = id;
        this.setChanged();
        notifyObservers();
    }

    public Bitmap getExerciseBitmap() {
        return currentExerciseImage;
    }

    public Integer getCurrentExerciseId() {
        return currentExerciseId;
    }

    public Integer getCountImg() {
        return countImg;
    }

    public Integer getCountImgMax() {
        return countImgMax;
    }

    public void setCurrentExerciseId(Integer id) {
        this.currentExerciseId = id;
    }
}
