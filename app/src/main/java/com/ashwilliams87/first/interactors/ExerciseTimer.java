package com.ashwilliams87.first.interactors;

import android.util.Log;

import com.ashwilliams87.first.helpers.CounterAsyncTask;
import com.ashwilliams87.first.interfaces.IAsyncTaskEvents;

import java.util.Observable;

public class ExerciseTimer extends Observable implements IAsyncTaskEvents {

    private static final String TAG = "ExerciseTimer";
    private Integer startTime = 0;

    private CounterAsyncTask counterAsyncTask;

    public ExerciseTimer(Integer startTime) {
        this.startTime = startTime;
    }

    @Override
    public void onPreExecute() {

    }

    @Override
    public void onPostExecute() {
        Log.d(TAG, "onPostExecute");
        this.setChanged();
        notifyObservers();
    }

    @Override
    public void onProgressUpdate(Integer value) {
        Log.d(TAG, "onProgressUpdate");
        Log.d(TAG, counterAsyncTask.getCurrentTime().toString());
        this.setChanged();
        notifyObservers();
    }

    @Override
    public void onCancel() {
        Log.d(TAG, "onCancel");
//        this.setChanged();
//        notifyObservers();
    }

    public void addTime(Integer n) {
        counterAsyncTask.addTime(n);
    }

    public void removeTime(Integer n) {
        counterAsyncTask.removeTime(n);
    }

    public void execute() {
        Log.d(TAG, "execute");
        this.counterAsyncTask = new CounterAsyncTask(this, this.startTime, 0);
        counterAsyncTask.execute();
    }

    public void cancel() {
        if (counterAsyncTask != null) {
            counterAsyncTask.cancel(true);
        }
    }

    public Integer getTime() {
        return counterAsyncTask.getCurrentTime();
    }
}
