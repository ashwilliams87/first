package com.ashwilliams87.first.interactors;

import com.ashwilliams87.first.interfaces.Observer;
import com.ashwilliams87.first.interfaces.Observable;

public class Subject implements Observable {
    //выпелить
    private Observer observer;

    public Subject(Observer observer) {
        this.registerObserver(observer);
    }

    @Override
    public void registerObserver(Observer observer) {
        this.observer = observer;
    }

    @Override
    public void removeObserver() {
        this.observer = null;
    }

    @Override
    public void notifyObserver(Object value) {

    }

}

