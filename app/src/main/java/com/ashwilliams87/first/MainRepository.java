package com.ashwilliams87.first;

import android.graphics.Bitmap;
import android.util.Log;

import com.ashwilliams87.first.interfaces.MainContract;

public class MainRepository implements MainContract.Repository {

    private static final String TAG = "MainRepository";

    @Override
    public String loadMessage() {
        Log.d(TAG, "loadMessage()");
        return "Упражнение №1";
    }

    @Override
    public Bitmap getExerciseBitmap() {
        return null;
    }

    @Override
    public void initExerciseDatabase() {

    }


}
