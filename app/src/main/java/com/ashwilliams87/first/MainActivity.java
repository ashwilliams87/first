package com.ashwilliams87.first;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ashwilliams87.first.interactors.ExerciseImage;
import com.ashwilliams87.first.interactors.ExerciseTimer;
import com.ashwilliams87.first.interfaces.MainContract;
import com.ashwilliams87.first.presenters.MainPresenter;
import com.ashwilliams87.first.repositories.ExerciseRepository;
import com.ashwilliams87.first.handlers.ExerciseImageHandler;

public class MainActivity extends AppCompatActivity implements MainContract.View, LocationListener {

    private static final String TAG = "MainActivity";
    private MainContract.Presenter mainPresenter;
    //view
    private TextView messageTextView;
    private TextView timerTextView;
    private ImageView exerciseImageView;
    public Handler exerciseHandler;

    SharedPreferences prefs = null;

    //кнопки
//    private Button startButton;
//    private Button resetButton;
//    private Button timerUpButton;
//    private Button timerDownButton;
//    private Button nextExerciseButton;
//    private Button prevExerciseButton;

    protected LocationManager locationManager;
    protected LocationListener locationListener;
    protected Context context;
    TextView txtLat;
    String lat;
    String provider;
    protected String latitude, longitude;
    protected boolean gps_enabled, network_enabled;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //Создаём Presenter и в аргументе передаём ему this - эта Activity  реализует интерфейс MainContract.View

        exerciseImageView = findViewById(R.id.image_view_exercise);

        mainPresenter = new MainPresenter(
                this,
                new ExerciseTimer(65),
                new ExerciseImage(getApplicationContext(), ExerciseRepository.getInstance(getApplicationContext())),
                new ExerciseImageHandler(exerciseImageView)
        );

        timerTextView = findViewById(R.id.text_view_timer);
        messageTextView = findViewById(R.id.message_text_view);
        Button startButton = findViewById(R.id.button_start);
        Button resetButton = findViewById(R.id.button_reset_exercise);
        Button timerUpButton = findViewById(R.id.button_timer_up);
        Button timerDownButton = findViewById(R.id.button_timer_down);
        Button nextExerciseButton = findViewById(R.id.button_next_exercise);
        Button prevExerciseButton = findViewById(R.id.button_prev_exercise);
        Button test1Button = findViewById(R.id.button_test_firbase);
        Button test2Button = findViewById(R.id.button_test_firbase_2);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        //https://stackoverflow.com/questions/32491960/android-check-permission-for-locationmanager?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
//        try {
//            if (ContextCompat.checkSelfPermission(this.getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION)
//                    != PackageManager.PERMISSION_GRANTED) {
//                Log.d(TAG, "Эх, нихера себе!");
//            } else {
//                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
//                Location loc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
//                Log.d(TAG, "Latitude: " + loc.getLatitude() + "\nLongitude: " + loc.getLongitude());
//            }
//        } catch (SecurityException e) {
//            Log.d(TAG, "Эх, нихера себе!");
//        }

        prefs = getSharedPreferences("com.ashwilliams87.first", MODE_PRIVATE);
        Log.d(TAG, "Main InitDB");
        //mainPresenter.initDatabase(getApplicationContext());

        if (prefs.getBoolean("firstrun", true)) {
            mainPresenter.initDatabase();
            prefs.edit().putBoolean("firstrun", false).apply();
        }

        //this.setExerciseImageView(mainPresenter.);

        //старт таймера текущего упражнения
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainPresenter.onClickStartButton();
            }
        });

        //сброс таймера и упражнения
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainPresenter.onClickResetButton();
            }
        });
        //увеличить таймер
        timerUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainPresenter.onClickTimerUpButton();
            }
        });

        //уменьшить таймер
        timerDownButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainPresenter.onClickTimeDownButton();
            }
        });
        //следующее упражнение
        nextExerciseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainPresenter.onClickNextExerciseButton();
            }
        });
        //предыдущее упражнения
        prevExerciseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainPresenter.onClickPrevExerciseButton();
            }
        });
        //предыдущее упражнения
        test1Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainPresenter.onClickTest1Button();
            }
        });

        //предыдущее упражнения
        test2Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainPresenter.onClickTest2Button();
            }
        });


        Log.d(TAG, "onCreate()");
    }

    @Override
    public void showText(String message) {
        messageTextView.setText(message);
        Log.d(TAG, "showMessage()");
    }

    @Override
    public void showTime(String time) {
        timerTextView.setText(time);
        Log.d(TAG, "showTime()");
        Log.d(TAG, time);
    }

    @Override
    public void setExerciseImageView(Bitmap exerciseImage) {
        exerciseImageView.setImageBitmap(exerciseImage);

    }

    @Override
    public int getExerciseImageViewId(int cImg) {
//        if (exerciseImageView != null) {
//            return exerciseImageView.getResources();
//        }
        return 0;
    }

    @Override
    public String getTimerValue() {
        return timerTextView.getText().toString();
    }

    //Вызываем у Presenter метод onDestroy, чтобы избежать утечек контекста и прочих неприятностей.
    @Override
    public void onDestroy() {
        super.onDestroy();
        mainPresenter.onDestroy();
        Log.d(TAG, "onDestroy()");
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "Latitude:" + location.getLatitude() + ", Longitude:" + location.getLongitude());
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
