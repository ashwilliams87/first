package com.ashwilliams87.first.daos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.ashwilliams87.first.entities.ExerciseEntity;

import java.util.List;

@Dao
public interface ExerciseDao {

    @Query("SELECT * FROM exercise")
    List<ExerciseEntity> getAll();

    @Query("SELECT * FROM exercise where exercise.id = :exerciseId")
    ExerciseEntity findById(long exerciseId);

    @Query("SELECT COUNT(*) from exercise")
    int countUsers();

    @Insert
    void insertAll(ExerciseEntity... exercises);

    @Delete
    void delete(ExerciseEntity exercise);
}
