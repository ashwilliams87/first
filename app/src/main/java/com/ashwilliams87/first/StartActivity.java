package com.ashwilliams87.first;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;

import com.ashwilliams87.first.presenters.StartPresenter;


public class StartActivity extends AppCompatActivity {

    private String TAG = "StartActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        //если первый запуск, создаем БД упражнений
        SharedPreferences prefs = getSharedPreferences("com.ashwilliams87.first", MODE_PRIVATE);
        //mainPresenter.initDatabase(getApplicationContext());
        if (prefs.getBoolean("firstrun", true)) {
            Log.d(TAG, "Database Initializaion started!");
            StartPresenter startPresenter = new StartPresenter();
            startPresenter.initDatabase();
            prefs.edit().putBoolean("firstrun", false).apply();
        }

        Button startEyeButton = findViewById(R.id.button_start_exercise_eye_activity);
        Button startSandovButton = findViewById(R.id.button_start_exercise_sandov_activity);
        Button startSettingsSandov = findViewById(R.id.button_settings_sandov_activity);
        Button startSettingsEye = findViewById(R.id.button_settings_eye_activity);

    }


}
