package com.ashwilliams87.first.helpers;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.util.Log;

import com.ashwilliams87.first.interfaces.Api;
import com.ashwilliams87.first.repositories.ExerciseRepository;
import com.ashwilliams87.first.threads.InitDataBaseThread;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class RepositoryApp extends Application {

    private static Api exerciseApi;
    private static ExerciseRepository roomDb;

    @Override
    public void onCreate() {
        super.onCreate();

        Log.d("Repository Init", "InitDb Presenter");

        //инициализируем ретрофит
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://umorili.herokuapp.com") //Базовая часть адреса
                .addConverterFactory(GsonConverterFactory.create()) //Конвертер, необходимый для преобразования JSON'а в объекты
                .build();

        exerciseApi = retrofit.create(Api.class); //Создаем объект, при помощи которого будем выполнять запросы

        //инициализируем Рум
        roomDb = Room.databaseBuilder(this.getApplicationContext(), ExerciseRepository.class, "exercise-database")
                // allow queries on the main thread.
                // Don't do this on a real app! See PersistenceBasicSample for an example.
                .allowMainThreadQueries()
                .build();
        InitDataBaseThread myThread = new InitDataBaseThread(this);
        myThread.start();

    }

    public static Api getApi() {
        return exerciseApi;
    }

    public static ExerciseRepository getRoomDb() {
        return roomDb;
    }

}
