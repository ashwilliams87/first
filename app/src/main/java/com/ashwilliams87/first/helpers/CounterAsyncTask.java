package com.ashwilliams87.first.helpers;

import android.os.AsyncTask;
import android.os.SystemClock;

import com.ashwilliams87.first.interfaces.IAsyncTaskEvents;

public class CounterAsyncTask extends AsyncTask<Integer, Integer, Integer> {

    private IAsyncTaskEvents interactor;

    private static Integer start = 65;
    private static Integer end = 0;
    private volatile static Integer currentTime = 65;

    public CounterAsyncTask(IAsyncTaskEvents interactor, Integer start, Integer end) {
        this.interactor = interactor;
        CounterAsyncTask.start = start;
        CounterAsyncTask.end = end;
    }

    @Override
    protected Integer doInBackground(Integer... integers) {
        currentTime = start;
        while (currentTime >= end) {
            if (isCancelled()) {
                return currentTime;
            }
            publishProgress(currentTime);
            SystemClock.sleep(1000);
            currentTime = currentTime - 1;
        }
        return end;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(Integer integer) {
        super.onPostExecute(integer);
    }


    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        if (interactor != null) {
            interactor.onProgressUpdate(currentTime);
        }
    }

    @Override
    protected void onCancelled(Integer aInteger) {
        super.onCancelled(aInteger);
        if (interactor != null) {
            interactor.onCancel();
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        if (interactor != null) {
            interactor.onCancel();
        }
    }

    public void addTime(Integer i) {
        currentTime = currentTime + i;
    }

    public Integer getCurrentTime() {
        return currentTime;
    }

    public void removeTime(Integer i) {
        currentTime = currentTime - i;
        if (currentTime < 0) {
            currentTime = 0;
        }
    }
}